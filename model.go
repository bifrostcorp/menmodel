package menmodel

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"
)

var SessionExpiredErr = errors.New("session expired")

type ErrorResponse struct {
	//	V   string `json:"v"`
	Err string `json:"Error,omitempty"`
}

type UserPreferencesStatusType int

const (
	New UserPreferencesStatusType = iota
	Accepted
	Rejected
)

type UserType int

const (
	Admin UserType = iota
	Mentor
	Mentee
	ProjectAdmin
)

func GetTypeString(ut UserType) string {
	switch ut {
	case Admin:
		return "Admin"
	case Mentor:
		return "Mentor"
	case Mentee:
		return "Mentee"
	case ProjectAdmin:
		return "Projec Admin"
	}
	return ""
}

type FieldType int

const (
	LONGSTRING FieldType = iota
	STRING
	SELECT
	MULTISELECT
	NUMBER
	DATE
	EMAIL
)

type ObjectiveType int

const (
	PROJECT_OBJECTIVE ObjectiveType = iota
	USER_OBJECTIVE
)

type MessageStatus int

const (
	AWAITING_MESSAGE_STATUS MessageStatus = iota
	ACCEPTED_MESSAGE_STATUS
	REJECTED_MESSAGE_STATUS
)

type UserProjectStatus int

const (
	NEW UserProjectStatus = iota
	CONFIRMED
	ASSIGNED
	PREFERENCE_SELECTED
	CLOSED
)

type Summary struct {
	Name        string `json:"name"`
	Description string `json:"description" gorm:"type:varchar(2000)"`
}

type Field struct {
	ID                uint    `gorm:"primary_key" json:"id,string"`
	UserID            uint    `json:"-"`
	ProjectID         uint    `json:"-"`
	FieldDefinitionID uint    `json:"field_definition_id"`
	Name              string  `json:"name"  gorm:"-"`
	StringValue       *string `json:"-"`
	LongStringValue   *string `json:"-" gorm:"type:varchar(2000);`
	IntValue          *int64  `json:"-"`
	//FloatValue        *float64    `json:"-" gorm:"type:float"`
	TimeValue *time.Time  `json:"-"`
	JSONValue *[]byte     `json:"-"`
	Value     interface{} `json:"value" gorm:"-"`
	Type      FieldType   `json:"type"`
}
type SectionData struct {
	ID     uint    `gorm:"primary_key" json:"id,string"`
	Name   string  `json:"name"`
	Fields []Field `json:"fields"`
}

func (f *Field) UnmarshalJSON(data []byte) error {
	jf := JSONField{}
	if err := json.Unmarshal(data, &jf); err != nil {
		return err
	}
	f.FieldDefinitionID = jf.FieldDefinitionID
	f.Type = jf.Type
	if v, ok := jf.Value.(string); ok && f.Type == STRING {
		f.StringValue = &v
	} else if v, ok := jf.Value.(string); ok && f.Type == LONGSTRING {
		f.LongStringValue = &v
	} else if f.Type == NUMBER {
		if v, ok := jf.Value.(int64); ok {
			f.IntValue = &v
		} else if v, ok := jf.Value.(string); ok {
			if i, err := strconv.ParseInt(v, 10, 64); err != nil {
				return err
			} else {
				f.IntValue = &i
			}
		}
	} else if v, ok := jf.Value.(time.Time); ok && f.Type == DATE {
		f.TimeValue = &v
	} else if v, ok := jf.Value.(string); ok && f.Type == SELECT {
		f.StringValue = &v
	} else if v, ok := jf.Value.([]interface{}); ok && f.Type == MULTISELECT {
		b, err := json.Marshal(v)
		if err != nil {
			return err
		}
		f.JSONValue = &b
	}

	return nil
}

type JSONField struct {
	FieldDefinitionID uint        `json:"field_definition_id,string"`
	Type              FieldType   `json:"type"`
	Name              string      `json:"name"`
	Value             interface{} `json:"value" gorm:"-"`
}

func (f *Field) MarshalJSON() ([]byte, error) {
	ef := JSONField{}
	switch f.Type {
	case LONGSTRING:
		if f.LongStringValue != nil {
			ef.Value = *f.LongStringValue
		}
	case STRING:
		if f.StringValue != nil {
			ef.Value = *f.StringValue
		}
	case NUMBER:
		if f.IntValue != nil {
			ef.Value = *f.IntValue
		}
	case DATE:
		if f.TimeValue != nil {
			ef.Value = *f.TimeValue
		}
	case MULTISELECT:
		if f.JSONValue != nil {
			ef.Value = json.RawMessage(*f.JSONValue)
		}
	case SELECT:
		if f.StringValue != nil {
			ef.Value = *f.StringValue
		}
	default:
		//return json.Marshal(f)
	}
	ef.Type = f.Type
	ef.Name = f.Name
	ef.FieldDefinitionID = f.FieldDefinitionID

	return json.Marshal(ef)
}

type Option struct {
	ID                uint   `gorm:"primary_key" json:"id,string"`
	FieldDefinitionID uint   `gorm:"ForeignKey:ID" json:"field_definition_id,string"`
	Value             string `json:"value"`
}

type FieldDefinition struct {
	Summary
	ID           uint        `gorm:"primary_key" json:"id,string"`
	SectionID    uint        `gorm:"ForeignKey:ID" json:"section_id,string"`
	Position     uint        `json:"position"`
	Type         FieldType   `json:"type"`
	Required     bool        `json:"required"`
	OtherAllowed bool        `json:"other_allowed"`
	Options      []Option    `json:"options,omitempty"`
	Value        interface{} `gorm:"-" json:"value,omitempty"`
}

type Section struct {
	Summary
	Position         uint              `json:"position"`
	ID               uint              `gorm:"primary_key" json:"id,string"`
	QuestionnaireID  uint              `gorm:"ForeignKey:id" json:"questionnaire_id,string"`
	FieldDefinitions []FieldDefinition `json:"field_definitions,omitempty"`
}

type Questionnaire struct {
	Summary
	ID        uint      `gorm:"primary_key" json:"id,string"`
	ProjectID uint      `json:"project_id,string"`
	Target    UserType  `json:"target"`
	Sections  []Section `json:"sections,omitempty"`
}

type Project struct {
	Summary
	ID            uint            `gorm:"primary_key" json:"id,string"`
	ClientID      uint            `json:"clientId,string"`
	Client        *Client         `json:"client,omitempty"`
	Mentor        *Questionnaire  `json:"mentor,omitempty"`
	Mentee        *Questionnaire  `json:"mentee,omitempty"`
	Objectives    []Objective     `json:"objectives,omitempty"`
	UsersProjects []UsersProjects `json:"usersProjects,omitempty"`
	Active        bool            `json:"active,omitempty"`
}

type ProjectSummary struct {
	Summary
	ID                  uint            `json:"id,string"`
	ClientID            uint            `json:"clientId,string"`
	Client              Client          `json:"client,string"`
	MentorsNo           int             `json:"mentors_no"`
	MenteesNo           int             `json:"mentees_no"`
	MentorsAssignedNo   int             `json:"mentors_assigned"`
	MenteesAssignedNo   int             `json:"mentees_assigned"`
	MenteesSelectedNo   int             `json:"mentees_selected"`
	Objectives          []Objective     `json:"objectives,omitempty"`
	Users               []UsersProjects `json:"users,omitempty"`
	Active              bool            `json:"active"`
	FormURL             string          `json:"form_url,omitempty"`
	MenteeQuestionaryID *uint           `json:"mentee_questionary_id,string"`
	MentorQuestionaryID *uint           `json:"mentor_questionary_id,string"`
}
type Client struct {
	Summary
	ID       uint      `gorm:"primary_key" json:"id,string"`
	Projects []Project `json:"projects,omitempty"`
}

type Meeting struct {
	ID            uint          `gorm:"primary_key" json:"id,string"`
	Date          time.Time     `json:"date,omitempty"`
	EndDate       time.Time     `json:"end_date,omitempty"`
	OrganizerID   uint          `json:"organizer_id,string"`
	Organizer     *User         `json:"organizer"`
	ParticipantID uint          `json:"participant_id,string"`
	Participant   *User         `json:"participant,string"`
	Text          string        `json:"text,omitempty"`
	Location      string        `json:"location,omitempty"`
	Lat           int           `json:"lat,omitempty"`
	Lang          int           `json:"lang,omitempty"`
	Status        MessageStatus `json:"status"`
}

type Objective struct {
	ID          uint          `gorm:"primary_key" json:"id,string"`
	Summary     string        `json:"summary"`
	Parent      uint          `json:"parent,string"`
	CreatedByID uint          `json:"created_by_id,string" gorm:"ForeignKey:ID`
	CreatedBy   *User         `json:"created_by,omitempty"`
	AssignedID  *uint         `json:"assigned_id,string" gorm:"ForeignKey:ID`
	Assigned    *User         `json:"assigned,omitempty"`
	Type        ObjectiveType `json:"objective_type"`
	Date        time.Time     `json:"date"`
	Completed   bool          `json:"completed"`
}
type UsersProjects struct {
	ID              uint              `gorm:"primary_key" json:"id,string"`
	UserID          uint              `gorm:"ForeignKey:ID" json:"-"`
	ProjectID       uint              `gorm:"ForeignKey:ID" json:"-"`
	User            User              `json:"user,omitempty"`
	Project         Project           `json:"project,omitempty"`
	Status          UserProjectStatus `json:"status"`
	UserType        UserType          `json:"user_type,omitempty"`
	MentorID        uint              `json:"mentor_id,omitempty"`
	Mentees         []User            `json:"mentees,omitempty gorm:"ForeignKey:MentorID"`
	QuestionnaireID uint              `gorm:"ForeignKey:ID" json:"-"`
}
type User struct {
	ID          uint            `gorm:"primary_key" json:"id,string"`
	Email       string          `json:"email,omitempty"`
	Name        string          `json:"name,omitempty"`
	Surname     string          `json:"surname,omitempty"`
	Description string          `json:"description,omitempty"`
	Password    []byte          `json:"-"`
	IsAdmin     bool            `json:"is_admin"`
	Projects    []UsersProjects `gorm:"ForeignKey:UserID" json:"projects,omitempty"`
	Avatar      *Avatar         `gorm:"ForeignKey:ID" json:"avatar,omitempty"`
}

type Avatar struct {
	ID    uint   `gorm:"primary_key" json:"id,string"`
	Image []byte `json:"image"`
}
type MenteeMentorPreferences struct {
	ID        uint                      `gorm:"primary_key" json:"id,string"`
	ProjectID uint                      `gorm:"ForeignKey:ID" json:"project_id"`
	MenteeID  uint                      `gorm:"ForeignKey:ID" json:"-"`
	MentorID  uint                      `gorm:"ForeignKey:ID" json:"-"`
	Mentee    *User                     `json:"mentee,omitempty"`
	Mentor    *User                     `json:"mentor,omitempty"`
	Status    UserPreferencesStatusType `json:"status"`
}
type UserSession struct {
	User
	IsAdmin       bool
	ClientID      uint
	Code          string
	ProjectsRole  map[uint]UserType
	ProjectsRoles map[uint]map[UserType]struct{}
}

func (us *UserSession) LogUserType() []interface{} {
	logstrings := make([]interface{}, 0, len(us.ProjectsRole))
	for pid, utype := range us.ProjectsRole {
		logstrings = append(logstrings, fmt.Sprintf("req_user_project_%d_type", pid), GetTypeString(utype))
	}
	return logstrings
}

type FieldDefinitionData struct {
	ID               uint `gorm:"primary_key" json:",string"`
	FiedDefinitionID uint
	UserID           uint
	Type             FieldType
	StringValue      *string
	FloatValue       *float64
	DateValue        *time.Time
	ListValue        *[]byte
}

type UserSessionData struct {
	UserData *UserSession `json:"user_data"`
}

func GetUintFromString(s string) (uint, error) {
	if us, err := strconv.ParseInt(s, 10, 64); err != nil {
		return 0, err
	} else {
		return uint(us), nil
	}

}
func GetUintFromMap(vars map[string]string, s string) (uint, error) {
	if sid, ok := vars[s]; ok {
		return GetUintFromString(sid)
	} else {
		return 0, fmt.Errorf("%s is a required parameter", s)
	}

}

func GetUserDataFromCtx(ctx context.Context) (*UserSession, error) {
	if v := ctx.Value("user_data"); v != nil {
		if userSession, ok := v.(UserSession); ok {
			return &userSession, nil
		}
		fmt.Println("found value but can't parse:", v)
	}
	return nil, SessionExpiredErr
}
